#include <stdio.h>
#include "./include/point.h"

typedef enum { false, true } bool;

int main(){

	int check = false, okx, oky, okz;
	float x, y, z, distancia;
	point p1, p2, pMedio;

    printf("\n.--------------------------------------.");
    printf("\n|  Programa que calcula la distancia   |");
    printf("\n|  entre dos puntos en R3              |");
    printf("\n'--------------------------------------'\n");

    //P1
    do{
    	printf("\n    Coordenadas de P1: ");
    	
    	okx = 0;
    	while( okx != 1 ){
    		printf("\n      Ingrese x: ");
    		okx = scanf("%f", &x);
            getchar();
    		
            if ( okx != 1 )
    			printf("\n      Error! Ingrese un numero\n");
    	}
    	
    	oky = 0;
    	while( oky != 1 ){
    		printf("      Ingrese y: ");
    		oky = scanf("%f", &y); 
            getchar();
    		
            if ( oky != 1 )
    			printf("\n      Error! Ingrese un numero\n");
    	}
		
    	okz = 0;
    	while( okz != 1 ){
    		printf("      Ingrese z: ");
    		okz = scanf("%f", &z); 
            getchar();
    		
            if ( okz != 1 ) 
    			printf("\n      Error! Ingrese un numero\n");
    	}
    	
    	
    	if ( okx==1 && oky==1 && okz==1 ) {
    		p1 = crearPunto(x,y,z);
    		check = true;
    	}
    	else {
    		check = false;
    	}

    }while( check==false );



    //P2
    do{
    	printf("\n    Coordenadas de P2: ");
    	
    	okx = 0;
    	while( okx != 1 ){
    		printf("\n      Ingrese x: ");
    		okx = scanf("%f", &x);
            //printf("%d", okx);
            getchar();
    		
            if ( okx != 1 )
    			printf("\n      Error! Ingrese un numero\n");
    	}
    	
    	oky = 0;
    	while( oky != 1 ){
    		printf("      Ingrese y: ");
    		oky = scanf("%f", &y);
            getchar();
    		
            if ( oky != 1 )
    			printf("\n      Error! Ingrese un numero\n");
    	}
		
    	okz = 0;
    	while( okz != 1 ){
    		printf("      Ingrese z: ");
    		okz = scanf("%f", &z);
            getchar();
    		
            if ( okz != 1 ) 
    			printf("\n      Error! Ingrese un numero\n");
    	}
    	
    	
    	if ( okx==1 && oky==1 && okz==1 ) {
    		p2 = crearPunto(x,y,z);
    		check = true;
    	}
    	else {
    		check = false;
    	}

    }while( check==false );

    distancia = calcularDistancia(p1, p2);

    printf("\n    La distancia entre los puntos");
    printf("\n      P1 [%6.2f %6.2f %6.2f]", p1.coorX, p1.coorY, p1.coorZ);
    printf("\n      P2 [%6.2f %6.2f %6.2f]", p2.coorX, p2.coorY, p2.coorZ);
    printf("\n\n    es:%9.2f\n", distancia);

    
    pMedio = punto_medio(p1, p2);
    
    printf("\n    El punto medio entre los puntos");
    printf("\n      P1 [%6.2f %6.2f %6.2f]", p1.coorX, p1.coorY, p1.coorZ);
    printf("\n      P2 [%6.2f %6.2f %6.2f]", p2.coorX, p2.coorY, p2.coorZ);
    printf("\n\n    es: ");
    printf("\n      P  [%6.2f %6.2f %6.2f]", pMedio.coorX, pMedio.coorY, pMedio.coorZ);
    printf("\n\n");

    return 0;
}
