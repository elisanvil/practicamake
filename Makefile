CC = gcc
CFLAGS = -c -I. -lm

all: distance

distance: main.o point.o
	@$(CC) -o distance main.o point.o -I. -lm
	@echo "Hecho"

main.o: main.c ./include/point.h
	@$(CC) $(CFLAGS) main.c

point.o: point.c ./include/point.h
	@$(CC) $(CFLAGS) point.c

clean:
	@rm -rf *.o distance 
	@echo "Archivos .o eliminados"
